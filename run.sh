#!/usr/bin/env bash

cd crm-demo-api
./gradlew clean assemble
cd ..

docker-compose build
docker-compose up -d

import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ToastService {
  toasts: { message: string, style: string, delay?: number } [] = [];

  showError(message: string): void {
    this.show(message, 'bg-danger text-light');
  }

  show(message: string, style: string): void {
    this.toasts.push({ message, style });
  }

  remove(toast): void {
    this.toasts = this.toasts.filter(t => t !== toast);
  }
}

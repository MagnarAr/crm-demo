import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ToastService } from '../toast/toast.service';

@Injectable()
export class HttpErrorInterceptor implements HttpErrorInterceptor {

  constructor(private router: Router, private toastService: ToastService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((err: HttpErrorResponse) => {
        if (err.status === 401) {
          this.router.navigateByUrl('/login');
        } else if (err.status === 400 || err.status === 500) {
          this.toastService.showError('Something went wrong. Validate request details.');
        }
        return throwError(err);
      })
    );
  }
}

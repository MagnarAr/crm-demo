import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authenticated = false;

  constructor(private http: HttpClient) {
  }

  public authenticate(credentials, callback): void {

    const body = new HttpParams()
      .set('username', credentials.username)
      .set('password', credentials.password);

    this.http.post('/api/login',
      body,
      {headers: new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'})}
    )
      .subscribe((response: any) => {
        this.authenticated = true;
        return callback && callback();
      }, () => {
        this.authenticated = false;
      });
  }
}

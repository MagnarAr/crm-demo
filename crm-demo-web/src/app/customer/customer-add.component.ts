import { Component, OnInit } from '@angular/core';
import { CustomerService } from './customer.service';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { CountryService } from '../country/country.service';

@Component({
  selector: 'app-customer-add',
  templateUrl: './customer-add.component.html'
})
export class CustomerAddComponent implements OnInit {

  customerForm: FormGroup;
  countries$: Observable<string[]>;

  constructor(private customerService: CustomerService,
              private countryService: CountryService,
              private fb: FormBuilder,
              private router: Router) {
  }

  ngOnInit(): void {
    this.countries$ = this.countryService.fetchCountries();

    this.customerForm = this.fb.group({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      username: new FormControl('', Validators.required),
      email: new FormControl('', Validators.email),
      address: new FormControl('', Validators.required),
      country: new FormControl('', Validators.required)
    });
  }

  addCustomer(): void {
    this.customerService.addCustomer(this.customerForm.value).subscribe(() => {
      this.router.navigateByUrl('/customers');
    });
  }
}

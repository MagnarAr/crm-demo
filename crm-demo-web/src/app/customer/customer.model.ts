export interface Customer {
  id?: string;
  firstName: string;
  lastName: string;
  username: string;
  email?: string;
  address: string;
  country: string;
}

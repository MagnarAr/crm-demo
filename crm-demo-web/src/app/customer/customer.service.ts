import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Customer } from './customer.model';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private http: HttpClient) {
  }

  public fetchCustomers(): Observable<Customer[]> {
    return this.http.get<Customer[]>('/api/customers');
  }

  public fetchCustomerById(id: string): Observable<Customer> {
    return this.http.get<Customer>(`/api/customers/${id}`);
  }

  public addCustomer(customer: Customer): Observable<Customer> {
    return this.http.post<Customer>('/api/customers', customer);
  }

  public updateCustomer(id: string, customer: Customer): Observable<Customer> {
    return this.http.put<Customer>(`/api/customers/${id}`, customer);
  }
}

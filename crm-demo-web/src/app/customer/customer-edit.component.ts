import { Component, OnInit } from '@angular/core';
import { CustomerService } from './customer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CountryService } from '../country/country.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-customer-edit',
  templateUrl: './customer-edit.component.html'
})
export class CustomerEditComponent implements OnInit {

  customerForm: FormGroup;
  countries$: Observable<string[]>;
  customerId: string;

  constructor(private customerService: CustomerService,
              private route: ActivatedRoute,
              private countryService: CountryService,
              private fb: FormBuilder,
              private router: Router) {
  }

  ngOnInit(): void {
    this.countries$ = this.countryService.fetchCountries();
    this.customerId = this.route.snapshot.params.id;

    this.customerService.fetchCustomerById(this.customerId).subscribe(customer => {
      this.customerForm = this.fb.group({
        firstName: new FormControl(customer.firstName, Validators.required),
        lastName: new FormControl(customer.lastName, Validators.required),
        username: new FormControl(customer.username, Validators.required),
        email: new FormControl(customer.email, Validators.email),
        address: new FormControl(customer.address, Validators.required),
        country: new FormControl(customer.country, Validators.required)
      });
    });
  }

  updateCustomer(): void {
    this.customerService.updateCustomer(this.customerId, this.customerForm.value).subscribe(() => {
      this.router.navigateByUrl('/customers');
    });
  }
}

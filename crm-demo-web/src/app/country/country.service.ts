import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(private http: HttpClient) {
  }

  public fetchCountries(): Observable<string[]> {
    return this.http.get<string[]>('/api/countries');
  }
}

# Run the application
Run `./run.sh`

NB! The Spring application takes some time to get up and running, 
so in the beginning the nginx might sometimes respond 502 if the service is not up yet.

Example users that can be used are:
* `employee-1`
* `employee-2`
* `employee-3`

All employees have password `test`.

# SRINI Demo Application Requirements
* Make a Spring Boot application
    * Create 3 users into the database (username and password)
    * User has to log in with one of the users (user registration does not have to be implemented)
    * User is displayed list of clients that he has entered. User is not allowed to see other user clients. 
        (Sample html is provided with test assignment)
    * User can add and edit clients (Sample html is provided with test assignment)
    * When new client is saved validate all mandatory fields
    * Store all input data to database
    * Values of country select box has to be populated with data from database.

* Please provide us your project source code.
* Application has to run with 1 click. Use spring boot, embedded database 
    and some database migration tool (liquibase, flyway) to execute sql scripts on startup.

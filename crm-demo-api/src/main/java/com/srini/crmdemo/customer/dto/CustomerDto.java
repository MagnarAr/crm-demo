package com.srini.crmdemo.customer.dto;

import com.srini.crmdemo.common.country.Country;
import lombok.Data;
import lombok.Builder;

import java.util.UUID;

@Data
@Builder
public class CustomerDto {

	private UUID id;
	private String firstName;
	private String lastName;
	private String username;
	private String email;
	private String address;
	private Country country;

}

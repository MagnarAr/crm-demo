package com.srini.crmdemo.customer;

import com.srini.crmdemo.common.AuthenticationHelper;
import com.srini.crmdemo.customer.dto.CustomerCreationDto;
import com.srini.crmdemo.customer.dto.CustomerDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collection;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/customers")
public class CustomerController {

	private final CustomerService customerService;

	@GetMapping
	public Collection<CustomerDto> fetchCustomers() {
		var userId = AuthenticationHelper.getCurrentUserId();
		return customerService.findUserCustomers(userId);
	}

	@GetMapping("/{customerId}")
	public CustomerDto fetchCustomerById(@PathVariable UUID customerId) {
		var userId = AuthenticationHelper.getCurrentUserId();
		return customerService.findUserCustomer(userId, customerId);
	}

	@PostMapping
	public CustomerDto addCustomer(@RequestBody @Valid CustomerCreationDto dto) {
		var userId = AuthenticationHelper.getCurrentUserId();
		return customerService.addCustomer(userId, dto);
	}

	@PutMapping("/{customerId}")
	public CustomerDto updateCustomer(@PathVariable UUID customerId, @RequestBody @Valid CustomerCreationDto dto) {
		var userId = AuthenticationHelper.getCurrentUserId();
		return customerService.updateCustomer(userId, customerId, dto);
	}
}

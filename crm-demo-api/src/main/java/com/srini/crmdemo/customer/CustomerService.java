package com.srini.crmdemo.customer;

import com.srini.crmdemo.customer.dto.CustomerCreationDto;
import com.srini.crmdemo.customer.dto.CustomerDto;
import com.srini.crmdemo.user.CrmUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CustomerService {

	private final CrmUserRepository crmUserRepository;
	private final CustomerMapper customerMapper;
	private final CustomerRepository customerRepository;

	@Transactional(readOnly = true)
	public Collection<CustomerDto> findUserCustomers(UUID userId) {
		return customerRepository.findAllByUserId(userId).stream()
				.map(customerMapper::toDto)
				.collect(Collectors.toList());
	}

	@Transactional
	public CustomerDto addCustomer(UUID userId, CustomerCreationDto dto) {
		return crmUserRepository.findById(userId)
				.map(user -> customerMapper.toEntity(dto, user))
				.map(customerRepository::save)
				.map(customerMapper::toDto)
				.orElseThrow();
	}

	@Transactional
	public CustomerDto updateCustomer(UUID userId, UUID customerId, CustomerCreationDto dto) {
		return customerRepository.findByIdAndUserId(customerId, userId)
				.map(customerMapper.updateFields(dto))
				.map(customerRepository::save)
				.map(customerMapper::toDto)
				.orElseThrow();
	}

	@Transactional(readOnly = true)
	public CustomerDto findUserCustomer(UUID userId, UUID customerId) {
		return customerRepository.findByIdAndUserId(customerId, userId)
				.map(customerMapper::toDto)
				.orElseThrow();
	}
}

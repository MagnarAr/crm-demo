package com.srini.crmdemo.customer;

import com.srini.crmdemo.customer.dto.CustomerCreationDto;
import com.srini.crmdemo.customer.dto.CustomerDto;
import com.srini.crmdemo.user.CrmUser;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.function.Function;

@Component
public class CustomerMapper {

	public CustomerDto toDto(Customer customer) {
		return CustomerDto.builder()
				.id(customer.getId())
				.firstName(customer.getFirstName())
				.lastName(customer.getLastName())
				.username(customer.getUsername())
				.address(customer.getAddress())
				.email(customer.getEmail())
				.country(customer.getCountry())
				.build();
	}

	public Customer toEntity(CustomerCreationDto dto, CrmUser crmUser) {
		return toEntity(UUID.randomUUID(), dto, crmUser);
	}

	public Customer toEntity(UUID id, CustomerCreationDto dto, CrmUser crmUser) {
		var customer = new Customer();
		customer.setId(id);
		customer.setUser(crmUser);
		this.updateFields(dto).apply(customer);
		return customer;
	}

	public Function<Customer, Customer> updateFields(CustomerCreationDto dto) {
		return customer -> {
			customer.setFirstName(dto.getFirstName());
			customer.setLastName(dto.getLastName());
			customer.setUsername(dto.getUsername());
			customer.setAddress(dto.getAddress());
			customer.setEmail(dto.getEmail());
			customer.setCountry(dto.getCountry());
			return customer;
		};
	}
}

package com.srini.crmdemo.customer;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CustomerRepository extends JpaRepository<Customer, UUID> {

	List<Customer> findAllByUserId(UUID userId);
	Optional<Customer> findByIdAndUserId(UUID id, UUID userId);
}

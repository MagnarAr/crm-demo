package com.srini.crmdemo.customer;

import com.srini.crmdemo.common.country.Country;
import com.srini.crmdemo.user.CrmUser;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = "id")
@Table(name = "customer")
public class Customer {

	@Id
	private UUID id;

	@Column(nullable = false)
	private String firstName;

	@Column(nullable = false)
	private String lastName;

	@Column(nullable = false)
	private String username;

	@Column
	private String email;

	@Column(nullable = false)
	private String address;

	@Column(nullable = false)
  @Enumerated(value = EnumType.STRING)
	private Country country;

	@ManyToOne
	@JoinTable(
			name = "user_customers",
			joinColumns = @JoinColumn(name = "customer_id"),
			inverseJoinColumns = @JoinColumn(name = "crm_user_id")
	)
	private CrmUser user;
}

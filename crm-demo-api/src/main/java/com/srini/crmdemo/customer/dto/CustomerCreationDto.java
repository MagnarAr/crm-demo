package com.srini.crmdemo.customer.dto;

import com.srini.crmdemo.common.country.Country;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class CustomerCreationDto {

	private @NotEmpty String firstName;
	private @NotEmpty String lastName;
	private @NotEmpty String username;
	private @Email String email;
	private @NotEmpty String address;
	private @NotNull Country country;
}

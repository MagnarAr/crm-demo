package com.srini.crmdemo.common.country;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class CountryService {

	public List<Country> fetchCountries() {
		return Arrays.asList(Country.values());
	}
}

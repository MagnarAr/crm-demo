package com.srini.crmdemo.common;

import com.srini.crmdemo.user.CrmUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;

import java.util.Optional;
import java.util.UUID;

public class AuthenticationHelper {

	public static UUID getCurrentUserId() {
		var authentication = SecurityContextHolder.getContext().getAuthentication();
		return Optional.ofNullable(authentication)
				.map(Authentication::getPrincipal)
				.filter(CrmUser.class::isInstance)
				.map(CrmUser.class::cast)
				.map(CrmUser::getId)
				.orElseThrow(() -> new SessionAuthenticationException("Not authenticated"));
	}
}

package com.srini.crmdemo.common.country;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/countries")
public class CountryController {

	private final CountryService countryService;

	@GetMapping
	public Collection<Country> fetchCountries() {
		return countryService.fetchCountries();
	}
}

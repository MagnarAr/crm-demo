package com.srini.crmdemo.common.country;

public enum Country {
	ESTONIA,
	LATVIA,
	LITHUANIA,
	FINLAND,
	SWEDEN
}

package com.srini.crmdemo.user;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CrmUserDetailsService implements UserDetailsService {

	private final CrmUserRepository crmUserRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return Optional
				.ofNullable(crmUserRepository.findByUsername(username))
				.orElseThrow(() -> new UsernameNotFoundException(String.format("%s not found", username)));
	}
}

package com.srini.crmdemo.user;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CrmUserRepository extends JpaRepository<CrmUser, UUID> {

	CrmUser findByUsername(String username);
}

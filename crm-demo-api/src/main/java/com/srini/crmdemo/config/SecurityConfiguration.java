package com.srini.crmdemo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.servlet.http.HttpServletResponse;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http.csrf().disable()
				.cors().disable();

		http.formLogin()
				.loginProcessingUrl("/api/login")
				.successHandler((request, response, authentication) -> {
					response.setStatus(HttpServletResponse.SC_OK);
					response.setContentType(MediaType.APPLICATION_JSON_VALUE);
					request.getSession(true).setMaxInactiveInterval(60 * 10);
				})
				.failureHandler((request, response, exception) -> {
					response.setContentType(MediaType.APPLICATION_JSON_VALUE);
					response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Bad credentials.");
				});

		http.exceptionHandling().authenticationEntryPoint((request, response, exception) -> {
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
		});
	}

	@Override
	public void configure(WebSecurity web) {
		web.ignoring().antMatchers("/h2-console/**");
	}
}

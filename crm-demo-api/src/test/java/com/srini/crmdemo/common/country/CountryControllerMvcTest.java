package com.srini.crmdemo.common.country;

import com.srini.crmdemo.WebMcvTestBase;
import com.srini.crmdemo.WithCrmUser;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = CountryController.class)
class CountryControllerMvcTest extends WebMcvTestBase {

	@MockBean
	private CountryService countryService;

	@Test
	@WithCrmUser
	void getCountries() throws Exception {
		when(countryService.fetchCountries()).thenReturn(List.of(Country.values()));

		mockMvc.perform(get("/api/countries"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", containsInAnyOrder("ESTONIA", "LATVIA", "LITHUANIA", "FINLAND", "SWEDEN")));
	}
}

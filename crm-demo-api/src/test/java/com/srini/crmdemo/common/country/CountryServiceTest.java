package com.srini.crmdemo.common.country;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.srini.crmdemo.common.country.Country.ESTONIA;
import static com.srini.crmdemo.common.country.Country.FINLAND;
import static com.srini.crmdemo.common.country.Country.LATVIA;
import static com.srini.crmdemo.common.country.Country.LITHUANIA;
import static com.srini.crmdemo.common.country.Country.SWEDEN;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class CountryServiceTest {

	@InjectMocks
	private CountryService countryService;

	@Test
	void fetchCountries_returnsAllFiveCountries() {
		var countries = countryService.fetchCountries();

		assertThat(countries)
				.hasSize(5)
				.containsExactlyInAnyOrder(ESTONIA, LATVIA, LITHUANIA, SWEDEN, FINLAND);
	}
}

package com.srini.crmdemo;

import com.srini.crmdemo.user.CrmUser;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

import java.util.UUID;

final class WithCrmUserSecurityContextFactory implements WithSecurityContextFactory<WithCrmUser> {

	public SecurityContext createSecurityContext(WithCrmUser withUser) {

		CrmUser principal = new CrmUser();
		principal.setId(UUID.randomUUID());
		principal.setUsername(withUser.username());
		principal.setPassword(withUser.password());

		Authentication authentication = new UsernamePasswordAuthenticationToken(
				principal, principal.getPassword(), principal.getAuthorities());
		SecurityContext context = SecurityContextHolder.createEmptyContext();
		context.setAuthentication(authentication);
		return context;
	}
}

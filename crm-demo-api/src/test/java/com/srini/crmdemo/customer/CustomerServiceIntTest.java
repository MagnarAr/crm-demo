package com.srini.crmdemo.customer;

import com.srini.crmdemo.IntegrationTestBase;
import com.srini.crmdemo.common.country.Country;
import com.srini.crmdemo.customer.dto.CustomerCreationDto;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.NoSuchElementException;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Transactional
@SpringBootTest
class CustomerServiceIntTest extends IntegrationTestBase {

	@Resource
	private CustomerService customerService;

	@Resource
	private CustomerRepository customerRepository;

	@Test
	void whenUserDoesNotHaveCustomer_cannotQuerySpecificCustomerData() {
		var userId = UUID.fromString("0f823c54-f426-4103-85ce-6aedf85cc00f");

		assertThrows(NoSuchElementException.class, () -> customerService.findUserCustomer(userId, UUID.randomUUID()));
	}

	@Test
	void whenUserHasCustomer_canQuerySpecificCustomerData() {
		var userId = UUID.fromString("0f823c54-f426-4103-85ce-6aedf85cc00f");
		var customerId = UUID.fromString("95237c37-754c-443b-8e66-bd5f117038e4");

		var customer = customerService.findUserCustomer(userId, customerId);

		assertThat(customer.getId().toString()).isEqualTo("95237c37-754c-443b-8e66-bd5f117038e4");
		assertThat(customer.getFirstName()).isEqualTo("Saniya");
		assertThat(customer.getLastName()).isEqualTo("Morin");
		assertThat(customer.getUsername()).isEqualTo("saniya-morin-1");
		assertThat(customer.getEmail()).isEqualTo("saniya-morin-1@test.ee");
		assertThat(customer.getAddress()).isEqualTo("4553 Bottom Lane, Ripley, NY");
		assertThat(customer.getCountry()).isEqualTo(Country.SWEDEN);
	}

	@Test
	void whenUserHasNoCustomers_returnsEmptyCollection() {
		var customers = customerService.findUserCustomers(UUID.fromString("ea908d48-2db2-40e2-baf9-b62d5b1037b5"));

		assertThat(customers).isEmpty();
	}

	@Test
	void whenUserHasCustomers_returnsCustomersWithData() {
		var customers = customerService.findUserCustomers(UUID.fromString("0f823c54-f426-4103-85ce-6aedf85cc00f"));

		assertThat(customers).hasSize(2)
				.anySatisfy(customer -> {
					assertThat(customer.getId().toString()).isEqualTo("95237c37-754c-443b-8e66-bd5f117038e4");
					assertThat(customer.getFirstName()).isEqualTo("Saniya");
					assertThat(customer.getLastName()).isEqualTo("Morin");
					assertThat(customer.getUsername()).isEqualTo("saniya-morin-1");
					assertThat(customer.getEmail()).isEqualTo("saniya-morin-1@test.ee");
					assertThat(customer.getAddress()).isEqualTo("4553 Bottom Lane, Ripley, NY");
					assertThat(customer.getCountry()).isEqualTo(Country.SWEDEN);
				})
				.anySatisfy(customer -> {
					assertThat(customer.getId().toString()).isEqualTo("64b9a58f-2f1b-4ba6-a35f-990d366ed72f");
					assertThat(customer.getFirstName()).isEqualTo("Nicholas");
					assertThat(customer.getLastName()).isEqualTo("Miller");
					assertThat(customer.getUsername()).isEqualTo("nicholas-miller-1");
					assertThat(customer.getEmail()).isEqualTo("nicholas-miller-1@test.ee");
					assertThat(customer.getAddress()).isEqualTo("2299 Patterson Fork Road, Chicago, IL");
					assertThat(customer.getCountry()).isEqualTo(Country.LATVIA);
				});
	}

	@Test
	void whenUserDoesNotExist_cannotAddCustomer() {
		assertThrows(NoSuchElementException.class, () -> addCustomer(UUID.randomUUID()));
	}

	@Test
	void whenUserExistsAndCustomerDoesNotExist_cannotUpdateCustumerData() {
		var userId = UUID.fromString("0f823c54-f426-4103-85ce-6aedf85cc00f");

		assertThrows(NoSuchElementException.class,
				() -> customerService.updateCustomer(userId, UUID.randomUUID(), CustomerCreationDto.builder().build()));
	}

	@Test
	void whenUserExists_userCanAddNewCustomer() {
		var userId = UUID.fromString("0f823c54-f426-4103-85ce-6aedf85cc00f");

		var customerId = addCustomer(userId);

		var customer = customerRepository.findByIdAndUserId(customerId, userId).orElseThrow();
		assertThat(customer.getId()).isNotNull();
		assertThat(customer.getFirstName()).isEqualTo("first");
		assertThat(customer.getLastName()).isEqualTo("last");
		assertThat(customer.getUsername()).isEqualTo("username");
		assertThat(customer.getEmail()).isEqualTo("email");
		assertThat(customer.getAddress()).isEqualTo("address");
		assertThat(customer.getCountry()).isEqualTo(Country.FINLAND);
		assertThat(customer.getUser().getId()).isEqualTo(userId);
	}

	@Test
	void whenUserAndCustomerExist_userCanModifyCustomerData() {
		var userId = UUID.fromString("0f823c54-f426-4103-85ce-6aedf85cc00f");

		var customerId = addCustomer(userId);

		var dto = CustomerCreationDto.builder()
				.firstName("new-first")
				.lastName("new-last")
				.username("new-username")
				.email("new-email")
				.address("new-address")
				.country(Country.ESTONIA)
				.build();
		customerService.updateCustomer(userId, customerId, dto);

		var customer = customerRepository.findByIdAndUserId(customerId, userId).orElseThrow();
		assertThat(customer.getId()).isEqualTo(customerId);
		assertThat(customer.getFirstName()).isEqualTo("new-first");
		assertThat(customer.getLastName()).isEqualTo("new-last");
		assertThat(customer.getUsername()).isEqualTo("new-username");
		assertThat(customer.getEmail()).isEqualTo("new-email");
		assertThat(customer.getAddress()).isEqualTo("new-address");
		assertThat(customer.getCountry()).isEqualTo(Country.ESTONIA);
		assertThat(customer.getUser().getId()).isEqualTo(userId);
	}

	private UUID addCustomer(UUID userId) {
		var dto = CustomerCreationDto.builder()
				.firstName("first")
				.lastName("last")
				.username("username")
				.email("email")
				.address("address")
				.country(Country.FINLAND)
				.build();
		return customerService.addCustomer(userId, dto).getId();
	}
}

package com.srini.crmdemo.customer;

import com.srini.crmdemo.common.country.Country;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class CustomerMapperTest {

	@InjectMocks
	private CustomerMapper customerMapper;

	@Test
	void allFieldsMapped() {
		var customer = getCustomer();

		var dto = customerMapper.toDto(customer);

		assertThat(dto.getId()).isEqualTo(customer.getId());
		assertThat(dto.getFirstName()).isEqualTo(customer.getFirstName());
		assertThat(dto.getLastName()).isEqualTo(customer.getLastName());
		assertThat(dto.getUsername()).isEqualTo(customer.getUsername());
		assertThat(dto.getCountry()).isEqualTo(customer.getCountry());
		assertThat(dto.getAddress()).isEqualTo(customer.getAddress());
		assertThat(dto.getEmail()).isEqualTo(customer.getEmail());
	}

	private Customer getCustomer() {
		Customer customer = new Customer();
		customer.setId(UUID.randomUUID());
		customer.setFirstName("first name");
		customer.setLastName("last name");
		customer.setUsername("username");
		customer.setAddress("my address");
		customer.setCountry(Country.ESTONIA);
		customer.setEmail("random email");
		return customer;
	}

}

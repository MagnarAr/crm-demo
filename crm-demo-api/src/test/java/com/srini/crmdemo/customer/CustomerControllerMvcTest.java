package com.srini.crmdemo.customer;

import com.srini.crmdemo.WebMcvTestBase;
import com.srini.crmdemo.WithCrmUser;
import com.srini.crmdemo.common.country.Country;
import com.srini.crmdemo.customer.dto.CustomerCreationDto;
import com.srini.crmdemo.customer.dto.CustomerDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = CustomerController.class)
class CustomerControllerMvcTest extends WebMcvTestBase {

	@MockBean
	private CustomerService customerService;

	@Test
	void whenNotAuthenticated_cannotQueryCustomers() throws Exception {
		mockMvc.perform(get("/api/customers")).andExpect(status().isUnauthorized());
	}

	@Test
	void whenNotAuthenticated_cannotQueryCustomer() throws Exception {
		mockMvc.perform(get("/api/customers/{id}", UUID.randomUUID())).andExpect(status().isUnauthorized());
	}

	@Test
	void whenNotAuthenticated_cannotAddCustomer() throws Exception {
		mockMvc.perform(post("/api/customers")
				.content(objectMapper.writeValueAsString(validCreationDto()))
				.contentType(MediaType.APPLICATION_JSON_VALUE)
		).andExpect(status().isUnauthorized());
	}

	@Test
	void whenNotAuthenticated_cannotModifyCustomer() throws Exception {
		mockMvc.perform(put("/api/customers/{id}", UUID.randomUUID())
				.content(objectMapper.writeValueAsString(validCreationDto()))
				.contentType(MediaType.APPLICATION_JSON_VALUE)
		).andExpect(status().isUnauthorized());
	}

	@Test
	@WithCrmUser
	void whenUserIsAuthenticatedAndHasCustomers_customersInfoIsReturned() throws Exception {
		var customerId = UUID.randomUUID();
		when(customerService.findUserCustomers(any())).thenReturn(List.of(validResponseDto(customerId)));

		mockMvc.perform(get("/api/customers"))
				.andExpect(jsonPath("$[0].id").value(customerId.toString()))
				.andExpect(jsonPath("$[0].firstName").value("first"))
				.andExpect(jsonPath("$[0].lastName").value("last"))
				.andExpect(jsonPath("$[0].username").value("username"))
				.andExpect(jsonPath("$[0].email").value("email"))
				.andExpect(jsonPath("$[0].address").value("address"))
				.andExpect(jsonPath("$[0].country").value("FINLAND"));
	}

	@Test
	@WithCrmUser
	void whenUserIsAuthenticatedAndHasCustomer_customerInfoIsReturnedWhenQueried() throws Exception {
		var customerId = UUID.randomUUID();
		when(customerService.findUserCustomer(any(), any())).thenReturn(validResponseDto(customerId));

		mockMvc.perform(get("/api/customers/{id}", customerId))
				.andExpect(jsonPath("$.id").value(customerId.toString()))
				.andExpect(jsonPath("$.firstName").value("first"))
				.andExpect(jsonPath("$.lastName").value("last"))
				.andExpect(jsonPath("$.username").value("username"))
				.andExpect(jsonPath("$.email").value("email"))
				.andExpect(jsonPath("$.address").value("address"))
				.andExpect(jsonPath("$.country").value("FINLAND"));
	}

	@Test
	@WithCrmUser
	void whenUserIsAuthenticated_canAddCustomerIfValidData() throws Exception {
		var dto = validCreationDto();

		mockMvc.perform(
				post("/api/customers")
						.content(objectMapper.writeValueAsString(dto))
						.contentType(MediaType.APPLICATION_JSON_VALUE)
		).andExpect(status().isOk());
	}

	@Test
	@WithCrmUser
	void whenUserIsAuthenticated_canAddCustomerIfEmailEmpty() throws Exception {
		var dto = validCreationDto();
		dto.setEmail(null);

		mockMvc.perform(
				post("/api/customers")
						.content(objectMapper.writeValueAsString(dto))
						.contentType(MediaType.APPLICATION_JSON_VALUE)
		).andExpect(status().isOk());
	}

	@Test
	@WithCrmUser
	void whenUserIsAuthenticated_cannotAddCustomerWhenEmailInvalid() throws Exception {
		var dto = validCreationDto();
		dto.setEmail("invalid-email");

		mockMvc.perform(
				post("/api/customers")
						.content(objectMapper.writeValueAsString(dto))
						.contentType(MediaType.APPLICATION_JSON_VALUE)
		).andExpect(status().isBadRequest());
	}

	@WithCrmUser
	@ParameterizedTest
	@CsvSource(value = {"firstName", "lastName", "username", "address", "country"})
	void whenUserIsAuthenticated_cannotAddCustomerWhenRequiredFieldMissing(String field) throws Exception {
		var dto = validCreationDto();
		ReflectionTestUtils.setField(dto, field, null);

		mockMvc.perform(
				post("/api/customers")
						.content(objectMapper.writeValueAsString(dto))
						.contentType(MediaType.APPLICATION_JSON_VALUE)
		).andExpect(status().isBadRequest());
	}

	@Test
	@WithCrmUser
	void whenUserIsAuthenticated_canModifyCustomerIfValidData() throws Exception {
		var dto = validCreationDto();

		mockMvc.perform(
				put("/api/customers/{id}", UUID.randomUUID())
						.content(objectMapper.writeValueAsString(dto))
						.contentType(MediaType.APPLICATION_JSON_VALUE)
		).andExpect(status().isOk());
	}

	@Test
	@WithCrmUser
	void whenUserIsAuthenticated_canModifyCustomerIfEmailEmpty() throws Exception {
		var dto = validCreationDto();
		dto.setEmail(null);

		mockMvc.perform(
				put("/api/customers/{id}", UUID.randomUUID())
						.content(objectMapper.writeValueAsString(dto))
						.contentType(MediaType.APPLICATION_JSON_VALUE)
		).andExpect(status().isOk());
	}

	@Test
	@WithCrmUser
	void whenUserIsAuthenticated_cannotModifyCustomerWhenEmailInvalid() throws Exception {
		var dto = validCreationDto();
		dto.setEmail("invalid-email");

		mockMvc.perform(
				put("/api/customers/{id}", UUID.randomUUID())
						.content(objectMapper.writeValueAsString(dto))
						.contentType(MediaType.APPLICATION_JSON_VALUE)
		).andExpect(status().isBadRequest());
	}

	@WithCrmUser
	@ParameterizedTest
	@CsvSource(value = {"firstName", "lastName", "username", "address", "country"})
	void whenUserIsAuthenticated_cannotModifyCustomerWhenRequiredFieldMissing(String field) throws Exception {
		var dto = validCreationDto();
		ReflectionTestUtils.setField(dto, field, null);

		mockMvc.perform(
				put("/api/customers/{id}", UUID.randomUUID())
						.content(objectMapper.writeValueAsString(dto))
						.contentType(MediaType.APPLICATION_JSON_VALUE)
		).andExpect(status().isBadRequest());
	}

	private CustomerCreationDto validCreationDto() {
		return CustomerCreationDto
				.builder()
				.firstName("first")
				.lastName("last")
				.username("username")
				.email("email@test.ee")
				.address("address")
				.country(Country.FINLAND)
				.build();
	}

	private CustomerDto validResponseDto(UUID customerId) {
		return CustomerDto
				.builder()
				.id(customerId)
				.firstName("first")
				.lastName("last")
				.username("username")
				.email("email")
				.address("address")
				.country(Country.FINLAND)
				.build();
	}
}

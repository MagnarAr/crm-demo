package com.srini.crmdemo.user;

import com.srini.crmdemo.IntegrationTestBase;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.annotation.Resource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CrmUserDetailsServiceIntTest extends IntegrationTestBase {

	@Resource
	private CrmUserDetailsService crmUserDetailsService;

	@Test
	void whenUserDoesNotExist_exceptionIsThrown() {
		Executable loadUserExecutable = () -> crmUserDetailsService.loadUserByUsername("random-user");

		var exception = assertThrows(UsernameNotFoundException.class, loadUserExecutable);
		assertThat(exception.getMessage()).isEqualTo("random-user not found");
	}

	@Test
	void whenUserExists_returnsUserDetails() {
		UserDetails crmUser = crmUserDetailsService.loadUserByUsername("employee-1");

		assertThat(crmUser).isNotNull();
		assertThat(crmUser).isInstanceOf(CrmUser.class);
		assertThat(crmUser.getUsername()).isEqualTo("employee-1");
		assertThat(crmUser.getPassword()).isEqualTo("$2b$10$T9bbveCexuT52G5kVuruU.1t.tU7NEqWQJxGSraoxKZMMhe7lykae");
		assertThat(crmUser.getAuthorities()).isEmpty();
		assertThat(crmUser.isAccountNonExpired()).isTrue();
		assertThat(crmUser.isAccountNonLocked()).isTrue();
		assertThat(crmUser.isCredentialsNonExpired()).isTrue();
		assertThat(crmUser.isEnabled()).isTrue();
	}
}
